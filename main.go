package main

import (
	"golangrest/handler"
	"net/http"
)

func main()  {
	mux := http.NewServeMux()
	h := &handler.Handler{}
	mux.Handle("/records", h)
	mux.Handle("/in-memory", h)
	http.ListenAndServe(":8080", mux)
}
