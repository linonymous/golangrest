package model

type QueryRequest struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
	MinCount int `json:"minCount"`
	MaxCount int `json:"maxCount"`
}

type Record map[string]interface{}

type Records []Record

