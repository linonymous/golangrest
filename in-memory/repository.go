package in_memory

import "golangrest/data"

type repository struct {
	data *data.Data
}

func NewRepository(d *data.Data) repository {
	return repository{data:d}
}

func (r *repository) Get(key string) (string, error) {
	return r.data.Get(key)
}

func (r *repository) Put(key, value string) error {
	return r.data.Put(key, value)
}
