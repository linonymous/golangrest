package in_memory

type Repository interface {
	Get(key string) (string, error)
	Put(key, value string) error
}

type service struct {
	r Repository
}

func NewService(r Repository) service {
	return service{r: r}
}

func  (s *service) PutKey(key, value string) error {
	return s.r.Put(key, value)
}

func (s *service) GetKey(key string) (string, error) {
	return s.r.Get(key)
}
