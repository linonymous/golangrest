package in_memory

import (
	"golangrest/data"
	"reflect"
	"testing"
)

func TestNewRepository(t *testing.T) {
	type args struct {
		d *data.Data
	}
	tests := []struct {
		name string
		args args
		want repository
	}{
		{
			"TEST1",
			args{d:&data.Data{
				HashMap: map[string]string{"key": "value"},
			}},
			repository{data:&data.Data{
				HashMap: map[string]string{"key": "value"},
			}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewRepository(tt.args.d); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_repository_Get(t *testing.T) {
	type fields struct {
		data *data.Data
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			"TestFieldNotExist",
			fields{data:&data.Data{
				HashMap: map[string]string{},
			}},
			args{key:"key"},
			"",
			true,
		},
		{
			"TestFieldExist",
			fields{data:&data.Data{
				HashMap: map[string]string{"key": "value"},
			}},
			args{key:"key"},
			"value",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repository{
				data: tt.fields.data,
			}
			got, err := r.Get(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_repository_Put(t *testing.T) {
	type fields struct {
		data *data.Data
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"TestFieldNotExist",
			fields{data:&data.Data{
				HashMap: map[string]string{},
			}},
			args{key:"key", value:"value"},
			false,
		},
		{
			"TestFieldExist",
			fields{data:&data.Data{
				HashMap: map[string]string{"key": "value"},
			}},
			args{key:"key", value:"value"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &repository{
				data: tt.fields.data,
			}
			if err := r.Put(tt.args.key, tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Put() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}