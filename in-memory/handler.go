package in_memory

import (
	"encoding/json"
	"golangrest/response"
	"io/ioutil"
	"log"
	"net/http"
)

type PutRequest struct {
	Key string `json:"key"`
	Value string `json:"value"`
}
type Service interface {
	GetKey(key string) error
	PutKey(key, value string) error
}

type InMemoryHandler struct {
	s *service
}

func NewInMemoryHandler(s *service) InMemoryHandler {
	return InMemoryHandler{s:s}
}

func (i *InMemoryHandler) PutHandler(w http.ResponseWriter, r *http.Request)  {
	w.Header().Set("content-type", "application/json")
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res, _ := json.Marshal(response.NewErrorMessage(err.Error()))
		_, _ = w.Write(res)
		return
	}
	var data PutRequest
	err = json.Unmarshal(requestBody, &data)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	err = i.s.PutKey(data.Key, data.Value)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	return
}

func (i *InMemoryHandler) GetHandler(w http.ResponseWriter, r *http.Request)  {
	w.Header().Set("content-type", "application/json")
	params := r.URL.Query()
	key := params.Get("key")
	value, err := i.s.GetKey(key)
	if key == "" || value == "" || err != nil {
		w.WriteHeader(http.StatusNotFound)
		var res []byte
		if err != nil {
			res, err = json.Marshal(response.NewErrorMessage(err.Error()))
		} else {
			res, err = json.Marshal(response.NewErrorMessage("Key Not Found!"))
		}
		if err != nil {
			log.Println("GetHandler: ", err.Error())
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		_, _ = w.Write(res)
		return
	}
	
	res, err := json.Marshal(response.NewInMemoryResponse(key, value))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res, _ := json.Marshal(response.NewErrorMessage(err.Error()))
		_, _ = w.Write(res)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
	return
}