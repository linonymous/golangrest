package response

import "golangrest/model"

func NewInMemoryResponse(key, value string) *InMemoryResponse {
	return &InMemoryResponse{
		Key:   key,
		Value: value,
	}
}

func NewErrorMessage(s string) *ErrorMessage {
	return &ErrorMessage{Message:s}
}

func NewRecordResponse(response model.Records, code int, Message string) *RecordResponse {
	return &RecordResponse{
		Code:    code,
		Message: Message,
		Records: response,
	}
}