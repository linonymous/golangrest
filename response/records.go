package response

import "golangrest/model"

type RecordResponse struct {
	Code    int          `json:"code"`
	Message string       `json:"message"`
	Records model.Records `json:"records"`
}
