package response

type InMemoryResponse struct {
	Key string `json:"key"`
	Value string `json:"value"`
}
