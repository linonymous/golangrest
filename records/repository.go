package records

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golangrest/model"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
)

type repository struct {
	client *mongo.Client
}

func NewRepository(client *mongo.Client) repository {
	return repository{client:client}
}

func (r *repository) QueryRecords(request model.QueryRequest) (model.Records, error) {
	collection := r.client.Database("getir-case-study").Collection("records")
	startDate, _ := time.Parse("2006-01-02", request.StartDate)
	endDate, _ := time.Parse("2006-01-02", request.EndDate)

	projectStage := bson.M{"$project": bson.M{
		"_id": 1,
		"createdAt": "$createdAt",
		"totalCount": bson.M{
			"$sum": "$counts",
		},
	}}
	matchtStage := bson.M{"$match": bson.M{
		"totalCount": bson.M{
			"$gte": request.MinCount,
			"$lte": request.MaxCount,
		},
		"createdAt": bson.M{
			"$gte": primitive.NewDateTimeFromTime(startDate),
			"$lte": primitive.NewDateTimeFromTime(endDate),
		},
	}}
	result, err := collection.Aggregate(context.TODO(), []bson.M{projectStage, matchtStage})
	if err != nil {
		log.Println("Repository: Query Records failed due to ", err.Error())
		return nil, err
	}
	var response model.Records
	err = result.All(context.TODO(), &response)
	if err != nil {
		log.Println("Repository: Query Records failed due to ", err.Error())
		return nil, err
	}
	return response, nil
}

