package records

import (
	"encoding/json"
	"golangrest/model"
	"golangrest/response"
	"io/ioutil"
	"net/http"
)

type Service interface {
	Query([]byte) (model.Records, error)
}

type RecordHandler struct {
	s Service
}

func NewRecordHandler(s Service) RecordHandler {
	return RecordHandler{s:s}
}

func (i *RecordHandler) QueryHandler(w http.ResponseWriter, r *http.Request)  {
	w.Header().Set("content-type", "application/json")
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		res, _ := json.Marshal(response.NewRecordResponse(nil, 400, err.Error()))
		_, _ = w.Write(res)
		return
	}
	records, err := i.s.Query(requestBody)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		res, _ := json.Marshal(response.NewRecordResponse(records, 500, err.Error()))
		_, _ = w.Write(res)
		return
	}
	res, _ := json.Marshal(response.NewRecordResponse(records, 0, "Success"))
	_, _ = w.Write(res)
	w.WriteHeader(http.StatusOK)
}
