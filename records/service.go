package records

import (
	"encoding/json"
	"golangrest/model"
)

type Repository interface {
	QueryRecords(request model.QueryRequest) (model.Records, error)
}

type service struct {
	r Repository
}

func NewService(r Repository) service {
	return service{r:r}
}

func (s *service) Query(request []byte) (model.Records, error) {
	var requestObject model.QueryRequest
	err := json.Unmarshal(request, &requestObject)
	if err != nil {
		return nil, err
	}
	records, err := s.r.QueryRecords(requestObject)
	if err != nil {
		return nil, err
	}
	return records, nil
}
