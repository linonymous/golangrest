# Golang REST API #


* Consists of two APIs
    * /in-memory supports GET & POST
    * /records supports POST method

### How do I get set up? ###

* Export an environment variable named `MONGODB_URL` 
* Build using `go build`
* run the binary
