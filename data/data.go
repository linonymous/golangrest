package data

import (
	"errors"
	"fmt"
	"sync"
)

type Data struct {
	HashMap map[string]string
	mutex sync.RWMutex
}

func (d *Data) Get(key string) (string, error) {
	d.mutex.RLock()
	value, isPresent := d.HashMap[key]
	d.mutex.RUnlock()
	if !isPresent {
		return "", errors.New(fmt.Sprintf("Associated value with key %s not found", key))
	}
	return value, nil
}

func (d *Data) Put(key, value string) error {
	d.mutex.Lock()
	if d.HashMap == nil {
		d.HashMap = make(map[string]string)
	}
	d.HashMap[key] = value
	d.mutex.Unlock()
	return nil
}
