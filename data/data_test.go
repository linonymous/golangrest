package data

import (
	"sync"
	"testing"
)

func TestData_Get(t *testing.T) {
	type fields struct {
		HashMap map[string]string
		mutex   sync.RWMutex
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			"TestKeyDoesNotExist",
			fields{
				HashMap: make(map[string]string),
				mutex:   sync.RWMutex{},
			},
			args{key:"name"},
			"",
			true,
		},
		{
			"TestKeyExist",
			fields{
				HashMap: map[string]string{"name": "bob"},
				mutex:   sync.RWMutex{},
			},
			args{key:"name"},
			"bob",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Data{
				HashMap: tt.fields.HashMap,
				mutex:   tt.fields.mutex,
			}
			got, err := d.Get(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestData_Put(t *testing.T) {
	type fields struct {
		HashMap map[string]string
		mutex   sync.RWMutex
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"TestKeyAlreadyExist",
			fields{
				HashMap: map[string]string{"name":"bob"},
				mutex:   sync.RWMutex{},
			},
			args{
				key:   "name",
				value: "alice",
			},
			false,
		},
		{
			"TestKeyDoesNotExist",
			fields{
				HashMap: make(map[string]string),
				mutex:   sync.RWMutex{},
			},
			args{
				key:   "name",
				value: "alice",
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Data{
				HashMap: tt.fields.HashMap,
				mutex:   tt.fields.mutex,
			}
			if err := d.Put(tt.args.key, tt.args.value); (err != nil) != tt.wantErr {
				t.Errorf("Put() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}