package handler

import (
	"context"
	"encoding/json"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golangrest/data"
	in_memory "golangrest/in-memory"
	"golangrest/records"
	"golangrest/response"
	"net/http"
	"os"
	"strings"
)

var inMemoryHandler in_memory.InMemoryHandler
var recordHandler records.RecordHandler

func init()  {
	// Initialize in memory handler
	var d data.Data
	repo := in_memory.NewRepository(&d)
	service := in_memory.NewService(&repo)
	inMemoryHandler = in_memory.NewInMemoryHandler(&service)

	// initialize record handler
	URL := os.Getenv("MONGODB_URL")
	if URL == "" {
		panic("MONGODB_URL is empty, set via environment")
	}
	clientOptions := options.Client().ApplyURI(URL)

	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return
	}
	recordRepo := records.NewRepository(client)
	recordSerivce := records.NewService(&recordRepo)
	recordHandler = records.NewRecordHandler(&recordSerivce)
}

type Handler struct {}

func (h *Handler)  ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	if strings.HasPrefix(r.URL.Path, "/in-memory") {
		switch r.Method {
		case http.MethodGet:
			inMemoryHandler.GetHandler(w, r)
		case http.MethodPost:
			inMemoryHandler.PutHandler(w, r)
		}
	} else if strings.HasPrefix(r.URL.Path, "/records") {
		switch r.Method {
		case http.MethodPost:
			recordHandler.QueryHandler(w, r)
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
		res, _ := json.Marshal(response.NewErrorMessage("Method/Resource not found"))
		_, _ = w.Write(res)
	}
	return
}